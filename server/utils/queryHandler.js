let queryGetRoomUserResto = (user_id, restaurant_id) => {
  var sql =
    "SELECT core_room_chat.room_name AS room_name FROM ref_participants JOIN core_room_chat ON ref_participants.room_chat_id = core_room_chat.id WHERE user_id = '" +
    user_id +
    "' AND restaurant_id = '" +
    restaurant_id +
    "'";
  return sql;
};

let getRoomName = (res) => {
  return JSON.parse(JSON.stringify(res))[0].room_name;
};

let queryGetUserName = (user_id) => {
  var sql =
    "SELECT ref_user.name AS name FROM ref_participants JOIN ref_user ON ref_participants.user_id = ref_user.id WHERE user_id = '" +
    user_id +
    "'";
  return sql;
};

let getName = (res) => {
  return JSON.parse(JSON.stringify(res))[0].name;
};

let createRoom = (id, room_name) => {
  var created_at = new Date()
    .toISOString()
    .replace(/T/, " ")
    .replace(/\..+/, "");

  var sql =
    "INSERT INTO core_room_chat (id, room_name, created_at) VALUES ('" +
    id +
    "','" +
    room_name +
    "','" +
    created_at +
    "')";

  return sql;
};

let queryGetIdRoom = (room_name) => {
  var sql =
    "SELECT id FROM core_room_chat WHERE room_name = '" + room_name + "'";
  return sql;
};

let getIdRoom = (res) => {
  return JSON.parse(JSON.stringify(res))[0].id;
};

let queryCreateParticipants = (id, room_chat_id, user_id, restaurant_id) => {
  var created_at = new Date()
    .toISOString()
    .replace(/T/, " ")
    .replace(/\..+/, "");

  var sql =
    "INSERT INTO ref_participants (id, room_chat_id, user_id, restaurant_id, created_at) VALUES ('" +
    id +
    "', '" +
    room_chat_id +
    "','" +
    user_id +
    "','" +
    restaurant_id +
    "','" +
    created_at +
    "')";
  return sql;
};

let queryGetRestoName = (restaurant_id) => {
  var sql =
    "SELECT name FROM core_restaurant WHERE id = '" + restaurant_id + "'";
  return sql;
};

module.exports = {
  queryGetRoomUserResto,
  queryGetRestoName,
  getRoomName,
  queryGetUserName,
  getName,
  createRoom,
  queryGetIdRoom,
  getIdRoom,
  queryCreateParticipants,
};

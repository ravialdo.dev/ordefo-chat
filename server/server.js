const path = require("path");
const http = require("http");
const express = require("express");
const socketIO = require("socket.io");
const str = require("@supercharge/strings");
const mysql = require("mysql");
const { v4: uuidv4 } = require("uuid");

const { generateMessage, generateLocationMessage } = require("./utils/message");
const { isRealString } = require("./utils/isRealString");
const { Users } = require("./utils/users");
const {
  queryGetRoomUserResto,
  getRoomName,
  queryGetUserName,
  queryGetRestoName,
  getName,
  createRoom,
  queryGetIdRoom,
  getIdRoom,
  queryCreateParticipants,
} = require("./utils/queryHandler");

const { query } = require("express");

const publicPath = path.join(__dirname, "/../public/");
const port = process.env.PORT || 3000;
let app = express();
let server = http.createServer(app);
let io = socketIO(server);
let users = new Users();

app.use(express.static(publicPath));

let db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "ordefo_panel",
});

io.on("connection", (socket) => {
  socket.on("join", (params, callback) => {
    db.query(
      queryGetRoomUserResto(params.user_id, params.restaurant_id),
      (err, res) => {
        if (params.type == "from-user") {
          if (res.length) {
            console.log("User connected to chat");

            var data = {
              socketID: socket.id,
              room: getRoomName(res),
            };

            socket.join(data["room"]);
            users.removeUser(data["socketID"]);

            db.query(queryGetUserName(params.user_id), data, (err, res) => {
              if (err) throw err;

              userName = getName(res);
              users.addUser(data["socketID"], userName, data["room"]);

              io.to(data["room"]).emit(
                "updateUsersList",
                users.getUserList(data["room"])
              );

              socket.emit(
                "newMessage",
                generateMessage("Admin", "Welocome to " + data["room"])
              );
            });
          } else {
            console.log("New user connected to chat");

            var data = {
              socketID: socket.id,
              newRoomName: "SOCKET-ROOM-" + str.random(10),
            };

            var roomName = data["newRoomName"];

            db.query(createRoom(uuidv4(), roomName), roomName, (err, res) => {
              db.query(queryGetIdRoom(roomName), (err, res) => {
                var idRoom = getIdRoom(res);

                db.query(
                  queryCreateParticipants(
                    uuidv4(),
                    idRoom,
                    params.user_id,
                    params.restaurant_id
                  ),
                  (err, res) => {
                    if (err) throw err;
                    socket.join(roomName);

                    db.query(
                      queryGetUserName(params.user_id),

                      (err, res) => {
                        if (err) throw err;

                        userName = getName(res);
                        users.addUser(socket.id, userName, roomName);

                        io.to(roomName).emit(
                          "updateUsersList",
                          users.getUserList(roomName)
                        );

                        socket.emit(
                          "newMessage",
                          generateMessage("Admin", "Welocome to " + roomName)
                        );
                      }
                    );
                  }
                );
              });
            });
          }
        } else if (params.type == "from-restaurant") {
          if (res.length) {
            console.log("Restaurant connected to chat");

            var data = {
              socketID: socket.id,
              room: getRoomName(res),
            };

            socket.join(data["room"]);
            users.removeUser(data["socketID"]);

            db.query(
              queryGetRestoName(params.restaurant_id),
              data,
              (err, res) => {
                if (err) {
                  throw err;
                }

                let restoName = getName(res);
                users.addUser(data["socketID"], restoName, data["room"]);

                io.to(data["room"]).emit(
                  "updateUsersList",
                  users.getUserList(data["room"])
                );

                socket.emit(
                  "newMessage",
                  generateMessage("Admin", "Welocome to " + data["room"])
                );
              }
            );
          } else {
            console.log("New Restaurant connected to chat");

            var data = {
              socketID: socket.id,
              newRoomName: "SOCKET-ROOM-" + str.random(10),
            };

            var roomName = data["newRoomName"];

            db.query(createRoom(uuidv4(), roomName), roomName, (err, res) => {
              db.query(queryGetIdRoom(roomName), (err, res) => {
                var idRoom = getIdRoom(res);

                db.query(
                  queryCreateParticipants(
                    uuidv4(),
                    idRoom,
                    params.user_id,
                    params.restaurant_id
                  ),
                  (err, res) => {
                    if (err) throw err;
                    socket.join(roomName);

                    db.query(
                      queryGetRestoName(params.restaurant_id),

                      (err, res) => {
                        if (err) throw err;

                        restoName = getName(res);
                        users.addUser(socket.id, restoName, roomName);

                        io.to(roomName).emit(
                          "updateUsersList",
                          users.getUserList(roomName)
                        );

                        socket.emit(
                          "newMessage",
                          generateMessage("Admin", "Welocome to " + roomName)
                        );
                      }
                    );
                  }
                );
              });
            });
          }
        }
      }
    );
  });

  socket.on("createMessage", (message, callback) => {
    let user = users.getUser(socket.id);

    if (user && isRealString(message.text)) {
      io.to(user.room).emit(
        "newMessage",
        generateMessage(user.name, message.text)
      );
    }
    callback("This is the server:");
  });

  socket.on("createLocationMessage", (coords) => {
    let user = users.getUser(socket.id);

    if (user) {
      io.to(user.room).emit(
        "newLocationMessage",
        generateLocationMessage(user.name, coords.lat, coords.lng)
      );
    }
  });

  socket.on("disconnect", () => {
    let user = users.removeUser(socket.id);
    console.log(user.name + " has been left");

    if (user) {
      io.to(user.room).emit("updateUsersList", users.getUserList(user.room));
      io.to(user.room).emit(
        "newMessage",
        generateMessage(
          "Admin",
          `${user.name} has left ${user.room} chat room.`
        )
      );
    }
  });
});

server.listen(port, () => {
  console.log(`Server is up on port ${port}`);
});
